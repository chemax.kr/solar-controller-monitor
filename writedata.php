<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 07.12.2016
 * Time: 10:39
 */
$query2 = "INSERT INTO data 
(batt_voltage_v, pv_voltage_v, batt_current_a, charge_power_w, energy_gen_kwh, max_batt_voltage, min_batt_voltage,
batt_state, charge_state, soc_percent, remote_sensor_temp, local_sensor_temp, controller_id) 
VALUES
 (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

$stmt2 = $link2->prepare($query2);

$stmt2->bind_Param('dddddddssiddi', $dataarray[0], $dataarray[1], $dataarray[2], $dataarray[3], $dataarray[4],
    $dataarray[5], $dataarray[6], $dataarray[7], $dataarray[8], $dataarray[9], $dataarray[10],
    $dataarray[11], $controllerID);

$stmt2->execute();

$stmt2->close();