<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table id="example" class="table table-bordered table-striped table-hover table-condensed table-responsive" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>
            Напряжение аккумулятор(V):
        </th>
        <th>
            Напряжение от СП(V):
        </th>
        <th>
            Ток заряда(A):
        </th>
        <th>
            Мощность заряда(W):
        </th>
        <th>
            Сгенерированно электроэнергии(kWh):
        </th>
        <th>
            Макс.напряжение акк.(V):
        </th>
        <th>
            Мин.напряжение акк.(V):
        </th>
        <th>
            Состояние акк.:
        </th>
        <th>
            Состояние заряда:
        </th>
        <th>
            Общий уровень заряда (SOC)(%):
        </th>
        <th>
            Температура на внешн.датчике(oC):
        </th>
        <th>
            Температура на внутр.датчике(oC):
        </th>
        <th>
            Имя Контроллера
        </th>
        <th>
            Дата\время
        </th>

    </tr>
    </thead>
    <tbody>
    <?php
    $i=0;
    while ($data[$i]) {?>
    <tr align="center">
        <td>
            <?php echo $data[$i]['batt_voltage_v'];?>
        </td>
        <td>
            <?php echo $data[$i]['pv_voltage_v'];?>
        </td>
        <td>
            <?php echo $data[$i]['batt_current_a'];?>
        </td>
        <td>
            <?php echo $data[$i]['charge_power_w'];?>
        </td>
        <td>
            <?php echo $data[$i]['energy_gen_kwh'];?>
        </td>
        <td>
            <?php echo $data[$i]['max_batt_voltage'];?>
        </td>
        <td>
            <?php echo $data[$i]['min_batt_voltage'];?>
        </td>
        <td>
            <?php echo $data[$i]['batt_state'];?>
        </td>
        <td>
            <?php echo $data[$i]['charge_state'];?>
        </td>
        <td>
            <?php echo $data[$i]['soc_percent'];?>
        </td>
        <td>
            <?php echo $data[$i]['remote_sensor_temp'];?>
        </td>
        <td>
            <?php echo $data[$i]['local_sensor_temp'];?>
        </td>
        <td>
            <?php echo $data[$i]['name'];?>
        </td>
        <td>
            <font size:7px><?php echo $data[$i]['inserttime'];?></font>
        </td>

    </tr>
    <?php
    $i++;
    }?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>