<?php
$whereSQL = false;
if (isset($_GET['controller_id'])){
    $whereSQL=" WHERE controller_id = ".$_GET['controller_id'];
}
if (isset($_GET['datestart'])){
    $datestart = new DateTime($_GET['datestart']);
}
else{
    $datestart = new DateTime();
}
if (isset($_GET['dateend'])){
    $dateend = new DateTime($_GET['dateend']);
}
else{
    $dateend = new DateTime();
}

if (isset($_GET['timestart'])){
    $timestart = new DateTime($_GET['timestart']);
}
else{
    $timestart = new DateTime('0:0:0');
}
if (isset($_GET['timeend'])){
    $timeend = new DateTime($_GET['timeend']);
}
else{
    $timeend = new DateTime('23:59:59');
}

if ($whereSQL) {
    $whereSQL = $whereSQL." AND inserttime > '".$datestart->format('Y-m-d')." ".$timestart->format('H:m:s')."' AND inserttime < '"
        .$dateend->format('Y-m-d')." ".$timeend->format('H:m:s')."'";
}
else{
    $whereSQL = " WHERE inserttime > '".$datestart->format('Y-m-d')." ".$timestart->format('H:m:s')."' AND inserttime < '"
        .$dateend->format('Y-m-d')." ".$timeend->format('H:m:s')."'";
}

/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 07.12.2016
 * Time: 12:32
 */
include  'header.php';
include 'selector.php';
include 'conn.php';
$query = 'SELECT * FROM `data` LEFT JOIN `controllers` ON (`data`.`controller_id`=`controllers`.`id`)'.$whereSQL."  ORDER BY inserttime ASC";
echo "<p>".$query."</p>";
if ($result = mysqli_query($link, $query)) {

    $i=0;
    $pvBool = false ;
    $kwhBool = false;
    $ondayV;
    $timestampV;
    while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC) ){

        $data[$i] = $row;
        $timestamp[] = $row['inserttime'];
        $kwh[]= $row['energy_gen_kwh'];
        $pvV[] = $row['pv_voltage_v'];
        if ($row['pv_voltage_v'] != 0){
            $pvBool = true;
        }
        if ($row['energy_gen_kwh'] != 0){
            $kwhBool = true;
        }
//        print_r($row);
//        echo "<br>";
        $i++;
    }
//print_r($kwh);

    function kWhForDay($data){
//        echo "<pre>";
//        print_r($data);

//        echo "</pre>";
        $count = count($data);
        $allperiod = 0;
//        $onday[]="";
        for ($i=0;$i<$count;$i++){
            $allperiod += $data[$i]['energy_gen_kwh'];
            $label=substr($data[$i]['inserttime'],0,10);
//            $labelV=substr($data[$i]['inserttime'],0,15);
//            $timestampV[]=substr($data[$i]['inserttime'],0,15)."0:00";
//            echo "<p>".$labelV."</p>";
//            print_r($label);
        $onday[$label] += $data[$i]['energy_gen_kwh'];
        $ondayV[$labelV] += $data[$i]['pv_voltage_v'];
        $ondayVCount[$labelV] ++;

        }
//        arrayV[] = array_keys($ondayVCount);
        
        foreach(array_keys($ondayV) as $key){
//            echo $key;
            $ondayV[$key]=$ondayV[$key]/$ondayVCount[$key];
//            echo $ondayV[$key]."<br>";
        }
        return $onday;
    }
    function pvVForHour($data){
//        echo "<pre>";
//        print_r($data);

//        echo "</pre>";
        $count = count($data);
        $allperiod = 0;
//        $onday[]="";
        for ($i=0;$i<$count;$i++){
//            $allperiod += $data[$i]['energy_gen_kwh'];
//            $label=substr($data[$i]['inserttime'],0,10);
            $labelV=substr($data[$i]['inserttime'],0,14);
            $timestampV[]=substr($data[$i]['inserttime'],0,14).":00:00";
//            echo "<p>".$labelV."</p>";
//            print_r($label);
//        $onday[$label] += $data[$i]['energy_gen_kwh'];
        $ondayV[$labelV] += $data[$i]['pv_voltage_v'];
        $ondayVCount[$labelV] ++;

        }
//        arrayV[] = array_keys($ondayVCount);
        
        foreach(array_keys($ondayV) as $key){
//            echo $key;
            $ondayV[$key]=$ondayV[$key]/$ondayVCount[$key];
//            echo $ondayV[$key]."<br>";
        }
        return $ondayV;
    }
    $onday = kWhForDay($data);
    $ondayV = pvVForHour($data);
//    print_r($ondayV);
//    print_r($onday);

}
foreach ($onday as $row){
    $daily[]=$row;
}
foreach ($ondayV as $row){
    $dailyV[]=$row;
}
//print_r($dailyV);
//print_r($daily);
?>

<?php if ($pvBool){?>
    <canvas id="pvV" width="400" height="100"></canvas>
    <script>
        var ctx = document.getElementById("pvV");
        var pvV = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?php echo json_encode(array_keys($ondayV));?>,
                datasets: [{
                    label: 'Напряжение на солнечных панелях(V) усредненное за час.',

                    data: <?php echo json_encode($dailyV);?>,
                    backgroundColor: "rgba(75,192,192,1)",
                    borderColor: "rgba(0,0,192,1)",
                    pointBorderColor: "rgba(75,0,0,1)",
                    pointHoverRadius: 10,
                    pointRadius: 2,
                    borderWidth: 1
                }]
            },
            options: {

                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
<?php }
else {?>
dailyV нету.
<?php }?>

    <?php if ($kwhBool){?>
    <canvas id="kWhH" width="400" height="50"></canvas>
<script>
    var ctx = document.getElementById("kWhH");
    var kWhH = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?php echo json_encode($timestamp);?>,
            datasets: [{
                label: 'Сгенерированно электроэнергии(kWh) час.',

                data: <?php echo json_encode($kwh);?>,
                backgroundColor: "rgba(75,192,192,1)",
                borderColor: "rgba(0,0,192,1)",
                pointBorderColor: "rgba(75,0,0,1)",
                pointHoverRadius: 10,
                pointRadius: 2,
                borderWidth: 1
            }]
        },
        options: {

            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>

<canvas id="kWhDay" width="400" height="50"></canvas>
<script>
    var ctx = document.getElementById("kWhDay");
    var kWhDay = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode(array_keys($onday));?>,
            datasets: [{
                label: 'Сгенерированно электроэнергии(kWh) сут.',

                data: <?php echo json_encode($daily);?>,
                backgroundColor: "rgba(75,192,192,1)",
                borderColor: "rgba(0,0,192,1)",
                pointBorderColor: "rgba(75,0,0,1)",
                pointHoverRadius: 10,
                pointRadius: 2,
                borderWidth: 1
            }]
        },
        options: {

            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>
    <?php } ?>

<?php include 'tableparam.php';?>